FROM openjdk:11
ADD target/portal-configserver.jar portal-configserver.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "portal-configserver.jar"]